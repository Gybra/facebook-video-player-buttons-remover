# README #

This is a Chrome extension which will hides or shows video player buttons box over an uploaded Facebook video on Facebook (not working for YouTube videos on Facebook)

### How can i execute it? ###

* Clone the repo
* Go to the Chrome settings
* Go to the Extension section
* Click on load unpacked extension
* You're ready to go, you'll find an icon on the right upper corner on Chrome
* Now go to [Facebook](http://www.facebook.com)
* Play a Facebook video and click on the Facebook icon on the right upper corner
* You'll notice that the video has no more video player buttons box